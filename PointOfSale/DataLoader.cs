﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;

namespace PointOfSale
{
	// ProductData model
	public class ProductData
	{
		public string productCode;
		public int quantity;
		public double price;
	}

	/// <summary>
	/// DataLoader helper class
	/// Loads data from json and deserializes it,
	/// Returns a list of products in the shape of ProductData
	/// </summary>
	public class DataLoader
	{
		public List<ProductData> LoadJson(string filePath)
		{
			using (StreamReader r = new StreamReader(filePath))
			{
				string json = r.ReadToEnd();
				List<ProductData> productDatas = JsonConvert.DeserializeObject<List<ProductData>>(json);
				return productDatas;
			}
		}
	}

}
