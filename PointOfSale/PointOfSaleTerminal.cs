﻿using System;
using System.Collections.Generic;

namespace PointOfSale
{
	/// <summary>
	/// Point of sale scanning systems that accepts an arbitrary ordering of products, 
	/// and returns the total price for an entire shopping cart based on per unit or 
	/// volume prices as applicable.
	/// </summary>
	public class PointOfSaleTerminal
	{
		private IDictionary<string, Product> _registeredProducts; // productCode, product
		private List<string> _scannedProducts;

		public List<string> ScannedProducts
		{
			get{ return _scannedProducts; }
		}

		public IDictionary<string, Product> RegisteredProducts
		{
			get{ return _registeredProducts; }
		}

		public PointOfSaleTerminal()
		{
			_registeredProducts = new Dictionary<string, Product>();
			_scannedProducts = new List<string>();
		}

		/// <summary>
		/// Adds the product and its bulkprice to the RegisteredProducts dictionary
		/// If the item is already registered, add/update its bulkprices
		/// 
		/// TODO: check if product has a price for quantity = 1, if not, throw error
		/// or set price per unit for product to be bulkprice/quantity?
		/// </summary>
		/// <param name="productCode"></param>
		/// <param name="productPrice">price of the product for quantity = <param name="productPrice">quantity</param></param>
		public void SetPricing(string productCode, double productPrice, int quantity = 1)
		{
			bool isAlreadyRegistered = _registeredProducts.ContainsKey(productCode);
			
			if (isAlreadyRegistered)
			{
				// if the product is already registered, 
				// set the new product price 
				// WARNING: this overwrites the old product price if the same quantity of the price is already set
				_registeredProducts[productCode].AddPrice((decimal)productPrice, quantity);
			} else
			{
				_registeredProducts[productCode] = new Product(productCode, (decimal)productPrice, quantity);
			}
		}

		/// <summary>
		/// Removes the item from the RegisteredProducts dictionary
		/// </summary>
		/// <param name="productCode">code of the product to reset</param>
		public void ResetPricing(string productCode)
		{
			if (_registeredProducts.ContainsKey(productCode))
			{
				_registeredProducts.Remove(productCode);
			}
		}

		/// <summary>
		/// Checks if the product scanned is registered, 
		/// if so, add it to the scanned products list.
		/// </summary>
		/// <param name="productCode">code of the product to scan</param>
		public void ScanProduct(string productCode)
		{
			// Return a warning if the scanned product is not registered
			if (!_registeredProducts.ContainsKey(productCode))
			{
				Console.WriteLine(productCode + " does not have a price. Please register the product using SetPricing(). Item price not added to total.");
			} else
			{
				_scannedProducts.Add(productCode);
			}
		}

		/// <summary>
		/// Calculates the total cost of all the items in the scannedProducts list.
		/// </summary>
		public decimal CalculateTotal()
		{
			decimal total = 0;

			// while there are still products left in the scannedProducts List
			while (_scannedProducts.Count > 0)
			{
				// get the first product in the list
				string currentProductCode = _scannedProducts[0];
				Product product = _registeredProducts[currentProductCode];

				// get all products that have the same code
				List<string> sameProducts = _scannedProducts.FindAll((string productCode) => {
					return (productCode == currentProductCode);
				});
				// remove them from the scannedProducts list
				_scannedProducts.RemoveAll((string productCode) => {
					return (productCode == currentProductCode);
				});

				// calculate the subtotal of all the same products
				total += product.CalculateSubTotal(sameProducts.Count);
			}
			return total;
		}
	}
}
