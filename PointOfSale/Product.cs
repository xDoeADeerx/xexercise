﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PointOfSale
{
	/// <summary>
	/// Product class that has properties product code and
	/// _prices as quantity-price key-value pairs, 
	/// </summary>
	public class Product
	{
		private string _code;
		private IDictionary<int, decimal> _bulkPrices; // key: quantity, value: price

		public string Code
		{
			get { return _code; }
		}

		/// <summary>
		/// Checks the bulkprice of a specific quantity
		/// </summary>
		/// <param name="bulk">bulk to check the price of</param>
		/// <returns>the bulkprice of a specific quantity</returns>
		public decimal GetBulkPriceOfSpecificQuantity(int bulk)
		{
			return _bulkPrices[bulk];
		}

		public Product(string code, decimal price, int quantity = 1)
		{
			_code = code;
			_bulkPrices = new Dictionary<int, decimal>
			{
				{ quantity, price }
			};
		}

		/// <summary>
		/// adds the bulk-price key value pair
		/// updates the price if the quantity-price already exists
		/// </summary>
		/// <param name="price">price of product</param>
		/// <param name="quantity">if unspecified, defaults to 1</param>
		public void AddPrice(decimal price, int quantity = 1)
		{
			_bulkPrices[quantity] = price;
		}

		/// <summary>
		/// Calculates the cost for a product type based on given quantity</summary>
		/// <param name="quantity">total quantity of this product</param>
		public decimal CalculateSubTotal(int quantity)
		{
			decimal subtotal = 0;

			// get all the bulk prices of the current product
			// from smallest to biggest quantities
			List<int> bulkPrices = _bulkPrices.Keys.OrderBy(num => num).ToList();

			int quantitiesRemaining = quantity;

			while (bulkPrices.Count > 0 && quantitiesRemaining > 0)
			{
				// start from the largest quantity and slowly whittle down to 1.
				int currentIndex = bulkPrices.Count - 1;
				int currentQuantity = bulkPrices[currentIndex];
				bulkPrices.RemoveAt(currentIndex);

				// if there are still quantities remaining higher than minimum bulkPrice
				if (quantitiesRemaining >= currentQuantity)
				{
					// get the highest factor
					int units = (int)Math.Floor((float)quantitiesRemaining / currentQuantity);

					// count price for X units of quantity Y
					subtotal += (units * _bulkPrices[currentQuantity]);

					// remove the calculated quantity from the loop
					quantitiesRemaining -= (units * currentQuantity);
				}
				// if the quantity scanned is lesser than the minimum bulkPrice,
				// throw an error.
				// eg. if 1 item is scanned but minimum bulk price is 3.
				// eg. if 3 items are scanned but minimum bulk price is 2.
				else if (currentIndex == 0 && currentQuantity > quantitiesRemaining)
				{
					int unitsRequired = currentQuantity - quantitiesRemaining;
					Console.WriteLine("Minimum quantity for purchasing item is not met. Please add " + unitsRequired + " more unit(s) of item " + Code + ".");
				}
			}
			return subtotal;
		}
	}

}
