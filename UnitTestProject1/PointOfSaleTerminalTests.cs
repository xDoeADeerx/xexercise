﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PointOfSale;

namespace PointOfSaleTests
{
	/// <summary>
	/// Tests for function PointOfSaleTerminal.SetPricing(price, quantity)
	/// </summary>
	[TestClass]
	public class RegisteredListTest
	{
		/// <summary>
		/// Check that SetPricing() updates the registered list correctly
		/// </summary>
		[TestMethod]
		public void SetPricing_NewItem_UpdatesRegisteredList()
		{
			// Arrange
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);

			IDictionary<string, Product> expected = new Dictionary<string, Product> {
				{ "A", new Product("A", (decimal)1.25, 1) }
			};

			// Assert
			IDictionary<string, Product> actual = terminal.RegisteredProducts;
			Assert.AreEqual(
				expected["A"].Code, 
				actual["A"].Code, 
				"Registered Products have different names");
			Assert.AreEqual(
				expected["A"].GetBulkPriceOfSpecificQuantity(1), 
				actual["A"].GetBulkPriceOfSpecificQuantity(1), 
				"Registered Products have different prices");
		}

		/// <summary>
		/// Check that SetPricing() for an already registered item updates the registered list correctly
		/// </summary>
		[TestMethod] 
		public void SetPricing_OldItem_UpdatesRegisteredProductPrices()
		{
			// Arrange
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("A", 5.00, 6);

			Product expectedProduct = new Product("A", (decimal)1.25, 1);
			expectedProduct.AddPrice((decimal)3.00, 3);
			expectedProduct.AddPrice((decimal)5.00, 6);
			IDictionary<string, Product> expected = new Dictionary<string, Product> {
				{ "A", expectedProduct}
			};

			// Assert
			IDictionary<string, Product> actual = terminal.RegisteredProducts;
			Assert.AreEqual(
				expected["A"].GetBulkPriceOfSpecificQuantity(1), 
				actual["A"].GetBulkPriceOfSpecificQuantity(1),
				"Registered Products have different prices for quantity 1");
			Assert.AreEqual(
				expected["A"].GetBulkPriceOfSpecificQuantity(3), 
				actual["A"].GetBulkPriceOfSpecificQuantity(3), 
				"Registered Products have different prices for quantity 3");
			Assert.AreEqual(
				expected["A"].GetBulkPriceOfSpecificQuantity(6), 
				actual["A"].GetBulkPriceOfSpecificQuantity(6),
				"Registered Products have different prices for quantity 6");
		}

		/// <summary>
		/// Check that SetPricing() for multiple different items updates the registered list correctly
		/// </summary>
		[TestMethod]
		public void SetPricing_MultipleItems_UpdatesRegisteredProductPrices()
		{
			// Arrange
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);

			Product expectedProductA = new Product("A", (decimal)1.25);
			Product expectedProductB = new Product("B", (decimal)4.25);
			Product expectedProductC = new Product("C", (decimal)1.00);

			IDictionary<string, Product> expected = new Dictionary<string, Product> {
				{ "A", expectedProductA}, 
				{ "B", expectedProductB}, 
				{ "C", expectedProductC}
			};

			// Assert
			IDictionary<string, Product> actual = terminal.RegisteredProducts;

			Assert.AreEqual(
				expected["A"].Code,
				actual["A"].Code,
				"Registered Products have different Names");
			Assert.AreEqual(
				expected["B"].Code,
				actual["B"].Code,
				"Registered Products have different Names");
			Assert.AreEqual(
				expected["C"].Code,
				actual["C"].Code,
				"Registered Products have different Names");

			Assert.AreEqual(
				expected["A"].GetBulkPriceOfSpecificQuantity(1),
				actual["A"].GetBulkPriceOfSpecificQuantity(1),
				"Registered Products have different Prices");
			Assert.AreEqual(
				expected["B"].GetBulkPriceOfSpecificQuantity(1),
				actual["B"].GetBulkPriceOfSpecificQuantity(1),
				"Registered Products have different Prices");
			Assert.AreEqual(
				expected["C"].GetBulkPriceOfSpecificQuantity(1),
				actual["C"].GetBulkPriceOfSpecificQuantity(1),
				"Registered Products have different Prices");
		}
	}

	/// <summary>
	/// Tests for PointOfSaleTerminal.ScanProduct(productCode)
	/// </summary>
	[TestClass]
	public class ScannedListTest
	{
		/// <summary>
		/// Checks that ScanProduct() updates the ScannedProducts list in the correct order
		/// </summary>
		[TestMethod]
		public void ScanItems_UpdatesProductsList_InCorrectOrder()
		{
			// Arrange
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);

			List<string> expected = new List<string> {
				"D", "A", "B", "C"
			};

			// Act
			terminal.ScanProduct("D");
			terminal.ScanProduct("A");
			terminal.ScanProduct("B");
			terminal.ScanProduct("C");

			// Assert
			List<string> actual = terminal.ScannedProducts;
	
			CollectionAssert.AreEquivalent(expected, actual, "Scanned products list has incorrect items");
			CollectionAssert.AreEqual(expected, actual, "Scanned products list is in wrong order");
		}

		/// <summary>
		/// Checks that scanning one product updates the ScannedProducts list correctly
		/// </summary>
		[TestMethod]
		public void Scan_OneProduct_OneItem_UpdatesProductsList()
		{
			// Arrange
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);

			List<string> expected = new List<string> {
				"C"
			};

			// Act
			terminal.ScanProduct("C");

			// Assert
			List<string> actual = terminal.ScannedProducts;
			CollectionAssert.AreEqual(expected, actual, "Scanning only 1 item produces an incorrect Scanned products list");
		}

		/// <summary>
		/// Checks that scanning multiple times of the same productcode updates the ScannedProducts list correctly
		/// </summary>
		[TestMethod]
		public void Scan_OneProduct_MultipleItems_UpdatesProductsList()
		{
			// Arrange
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);

			List<string> expected = new List<string> {
				"A", "A", "A", "A"
			};

			// Act
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");

			// Assert
			List<string> actual = terminal.ScannedProducts;
			CollectionAssert.AreEqual(expected, actual, "Scanning only multiples of 1 product produces an incorrect Scanned products list");
		}

		/// <summary>
		/// Checks that scanning one item of different products updates the ScannedProducts list correctly
		/// </summary>
		[TestMethod]
		public void Scan_MultipleProducts_OneItem_UpdatesProductsList()
		{
			// Arrange
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);

			List<string> expected = new List<string> {
				"A", "B", "C", "D"
			};

			// Act
			terminal.ScanProduct("A");
			terminal.ScanProduct("B");
			terminal.ScanProduct("C");
			terminal.ScanProduct("D");

			// Assert
			List<string> actual = terminal.ScannedProducts;
			CollectionAssert.AreEqual(expected, actual, "Scanning only 1 item of multiple products produces an incorrect Scanned products list");
		}

		/// <summary>
		/// Checks that scanning multiple items of different products updates the ScannedProducts list correctly
		/// </summary>
		[TestMethod]
		public void Scan_MultipleProducts_MultipleItems_UpdatesProductsList()
		{
			// Arrange
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);

			List<string> expected = new List<string> {
				"A", "B", "B",
				"A", "D", "D",
				"D"
			};

			// Act
			terminal.ScanProduct("A");
			terminal.ScanProduct("B");
			terminal.ScanProduct("B");
			terminal.ScanProduct("A");
			terminal.ScanProduct("D");
			terminal.ScanProduct("D");
			terminal.ScanProduct("D");

			// Assert
			List<string> actual = terminal.ScannedProducts;
			CollectionAssert.AreEqual(expected, actual, "Scanning multiple items of multiple products produces an incorrect Scanned products list");
		}

		/// <summary>
		/// Checks that scanning an item that is not registered (no price has been set) throws a warning
		/// </summary>
		[TestMethod]
		public void Scan_UnregisteredItem_ThrowsWarning()
		{
			// Arrange
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);

			string expected = "B does not have a price. Please register the product using SetPricing(). Item price not added to total.".Replace("\r", "").Replace("\n", "");

			// Act
			string actual = "";

			// use this to test because it is currently using Console to show warning
			using (StringWriter stringWriter = new StringWriter())
			{
				Console.SetOut(stringWriter);
				terminal.ScanProduct("B");
				actual = stringWriter.ToString().Replace("\r", "").Replace("\n", "");
			}

			Assert.AreEqual(expected, actual, "Scan_UnregisteredItem_ThrowsWarning");
		}
	}

	/// <summary>
	/// Tests for PointOfSaleTerminal.CalculateTotal()
	/// </summary>
	[TestClass]
	public class CalculateTotal
	{
		/// <summary>
		/// Test for scenario ABCDABA
		/// </summary>
		[TestMethod]
		public void Calculate_ABCDABA_Total()
		{
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);
			decimal expected = (decimal)13.25;

			// Act
			terminal.ScanProduct("A");
			terminal.ScanProduct("B");
			terminal.ScanProduct("C");
			terminal.ScanProduct("D");
			terminal.ScanProduct("A");
			terminal.ScanProduct("B");
			terminal.ScanProduct("A");

			decimal actual = terminal.CalculateTotal();

			Assert.AreEqual(expected, actual, "Total incorrect for scenario ABCDABA");
		}

		/// <summary>
		/// Test for scenario 7Cs
		/// </summary>
		[TestMethod]
		public void Calculate_CCCCCCC_Total()
		{
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);
			decimal expected = (decimal)6;

			// Act
			terminal.ScanProduct("C");
			terminal.ScanProduct("C");
			terminal.ScanProduct("C");
			terminal.ScanProduct("C");
			terminal.ScanProduct("C");
			terminal.ScanProduct("C");
			terminal.ScanProduct("C");

			decimal actual = terminal.CalculateTotal();

			Assert.AreEqual(expected, actual, "Total incorrect for scenario CCCCCCC (7 Cs)");
		}

		/// <summary>
		/// Required test for scenario ABCD
		/// </summary>
		[TestMethod]
		public void Calculate_ABCD_total()
		{
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);
			decimal expected = (decimal)7.25;

			terminal.ScanProduct("A");
			terminal.ScanProduct("B");
			terminal.ScanProduct("C");
			terminal.ScanProduct("D");
			decimal actual = terminal.CalculateTotal();

			Assert.AreEqual(expected, actual, "Total incorrect for scenario ABCD");
		}

		/// <summary>
		/// Test for scenario with exactly 2bulkquantity and 2 singlequantity
		/// </summary>
		[TestMethod]
		public void Calculate_8As_total()
		{
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);
			decimal expected = (decimal)8.5;

			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			decimal actual = terminal.CalculateTotal();

			Assert.AreEqual(expected, actual, "Total incorrect for scenario 8 As");
		}

		/// <summary>
		/// Test for scenario with exactly 1singlequantity
		/// </summary>
		[TestMethod]
		public void Calculate_B_total()
		{
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);

			decimal expected = (decimal)4.25;

			terminal.ScanProduct("B");

			decimal actual = terminal.CalculateTotal();

			Assert.AreEqual(expected, actual, "Total incorrect for scenario B");
		}

		/// <summary>
		/// Test for scenario if nothing is scanned
		/// </summary>
		[TestMethod]
		public void Calculate_0_total()
		{
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);
			decimal expected = (decimal)0;

			decimal actual = terminal.CalculateTotal();

			Assert.AreEqual(expected, actual, "Total incorrect for scenario 0");
		}

		/// <summary>
		/// Test for scenario with exactly 2bulkquantities
		/// </summary>
		[TestMethod]
		public void Calculate_6C_total()
		{
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);
			decimal expected = (decimal)5;

			terminal.ScanProduct("C");
			terminal.ScanProduct("C");
			terminal.ScanProduct("C");
			terminal.ScanProduct("C");
			terminal.ScanProduct("C");
			terminal.ScanProduct("C");

			decimal actual = terminal.CalculateTotal();

			Assert.AreEqual(expected, actual, "Total incorrect for scenario 6C");
		}

		/// <summary>
		/// Test for scenario if same terminal is used twice
		/// </summary>
		[TestMethod]
		public void MoreThan1Transaction_GivesCorrectTotal()
		{
			PointOfSaleTerminal terminal = new PointOfSaleTerminal();
			terminal.SetPricing("A", 1.25);
			terminal.SetPricing("A", 3.00, 3);
			terminal.SetPricing("B", 4.25);
			terminal.SetPricing("C", 1.00);
			terminal.SetPricing("C", 5.00, 6);
			terminal.SetPricing("D", 0.75);
			decimal expected = (decimal)10.25;

			//scenario 1 = 13.25
			terminal.ScanProduct("A");
			terminal.ScanProduct("B");
			terminal.ScanProduct("C");
			terminal.ScanProduct("D");
			terminal.ScanProduct("A");
			terminal.ScanProduct("B");
			terminal.ScanProduct("A");
			decimal unusedResult = terminal.CalculateTotal();

			//scenario 1 = 10.25
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("A");
			terminal.ScanProduct("B");
			terminal.ScanProduct("C");
			terminal.ScanProduct("D");

			decimal actual = terminal.CalculateTotal();

			Assert.AreEqual(expected, actual, "Making more than one transaction using the same terminal gives an incorrect total");
		}
	}
}
