﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PointOfSale;
using System.IO;

namespace ProductTests
{
	/// <summary>
	/// Tests for function CalculateSubTotal(quantity)
	/// </summary>
	[TestClass]
	public class CalculatePriceForProductTest
	{

		/// <summary>
		/// Test for single unit
		/// </summary>
		[TestMethod]
		public void CalculateProduct_SingleUnit_Price()
		{
			Product product = new Product("A", (decimal)1.25);
			product.AddPrice((decimal)3, 3);
			product.AddPrice((decimal)5, 6);

			decimal expected = (decimal)1.25;

			decimal actual = product.CalculateSubTotal(1);

			Assert.AreEqual(expected, actual, "Product total incorrect for calculating single unit price");
		}

		/// <summary>
		/// Test for 2 single units
		/// </summary>
		[TestMethod]
		public void CalculateProduct_TwoSingleUnits_Price()
		{
			Product product = new Product("A", (decimal)1.25);
			product.AddPrice((decimal)3, 3);
			product.AddPrice((decimal)5, 6);

			decimal expected = (decimal)2.50;

			decimal actual = product.CalculateSubTotal(2);

			Assert.AreEqual(expected, actual, "Product total incorrect for calculating single unit price");
		}

		/// <summary>
		/// Test for single bulk price 
		/// </summary>
		[TestMethod]
		public void CalculateProduct_BiggestBulk_Price()
		{
			Product product = new Product("A", (decimal)1.25);
			product.AddPrice((decimal)3, 3);
			product.AddPrice((decimal)5, 6);

			decimal expected = (decimal)5;

			decimal actual = product.CalculateSubTotal(6);

			Assert.AreEqual(expected, actual, "Product total incorrect for calculating biggest bulk price");
		}

		/// <summary>
		/// Test for middle bulk price 
		/// </summary>
		[TestMethod]
		public void CalculateProduct_MidBulk_Price()
		{
			Product product = new Product("A", (decimal)1.25);
			product.AddPrice((decimal)3, 3);
			product.AddPrice((decimal)5, 6);

			decimal expected = (decimal)3;

			decimal actual = product.CalculateSubTotal(3);

			Assert.AreEqual(expected, actual, "Product total incorrect for calculating middle bulk price");
		}

		/// <summary>
		/// Test for multiple single and multiple bulk prices
		/// Using (3*6), (1*3), (1*1) quantities
		/// </summary>
		[TestMethod]
		public void CalculateProduct_SingleAndBulk_Price()
		{
			Product product = new Product("A", (decimal)1.25);
			product.AddPrice((decimal)3, 3);
			product.AddPrice((decimal)5, 6);

			decimal expected = (decimal)20.5;

			decimal actual = product.CalculateSubTotal(23);

			Assert.AreEqual(expected, actual, "Product total incorrect for calculating single and bulk price");
		}

		/// <summary>
		/// Check if the quantity scanned is lesser than the minimum bulkPrice, a warning will be thrown.
		/// </summary>
		[TestMethod]
		public void CalculateProduct_MinItemsScannedNotMet_ThrowsWarning()
		{
			// Arrange
			Product product = new Product("A", (decimal)1.00, 3); // 3units for $1

			string expected = "Minimum quantity for purchasing item is not met. Please add 2 more unit(s) of item A.".Replace("\r", "").Replace("\n", "");

			// Act
			string actual = "";

			// use this to test because it is currently using Console to show warning
			using (StringWriter stringWriter = new StringWriter())
			{
				Console.SetOut(stringWriter);
				product.CalculateSubTotal(1);
				actual = stringWriter.ToString().Replace("\r", "").Replace("\n", "");
			}

			Assert.AreEqual(expected, actual, "SetPricing_NoQuantity1PriceSet_ThrowsWarning");
		}

		/// <summary>
		/// Check that the price will always be calculated 
		/// eg. 
		/// Prices: 3 units for $1 or 5 units for $1.50
		/// Scanned: 6 units
		/// Total will be [5 units=$1.50, with warning shown for last unit]
		/// Many ways to calculate this, read README.md for why this way to calculate prices was chosen
		/// </summary>
		[TestMethod]
		public void CalculateProduct_MinItemsScannedNotMet_GivesCorrectTotal_AndThrowsWarning()
		{
			// Arrange
			Product product = new Product("A", (decimal)1.00, 3); // 3units for $1
			product.AddPrice((decimal)1.5, 5); // 5units for $1.5

			decimal expectedPrice = (decimal)1.5;
			string expectedWarning = "Minimum quantity for purchasing item is not met. Please add 2 more unit(s) of item A.".Replace("\r", "").Replace("\n", "");

			// Act
			string actualWarning = "";
			decimal actualPrice = 0;

			// use this to test because it is currently using Console to show warning
			using (StringWriter stringWriter = new StringWriter())
			{
				Console.SetOut(stringWriter);
				actualPrice = product.CalculateSubTotal(6);
				actualWarning = stringWriter.ToString().Replace("\r", "").Replace("\n", "");
			}

			Assert.AreEqual(expectedPrice, actualPrice, "SetPricing_NoQuantity1PriceSet_ThrowsWarning");
			Assert.AreEqual(expectedWarning, actualWarning, "SetPricing_NoQuantity1PriceSet_ThrowsWarning");
		}
	}

	/// <summary>
	/// Tests for function GetPriceForQuantity(bulkPrice)
	/// </summary>
	[TestClass]
	public class GetPriceForQuantityTest
	{
		/// <summary>
		/// Test for getting the price for bulkPrice = 1
		/// </summary>
		[TestMethod]
		public void GetPriceForQuantity_Single()
		{
			Product product = new Product("A", (decimal)1.25);
			decimal expected = (decimal)1.25;

			decimal actual = product.GetBulkPriceOfSpecificQuantity(1);

			Assert.AreEqual(expected, actual, "Price incorrect for quantity: 1");
		}

		/// <summary>
		/// Test for getting the price for bulkPrice = 6
		/// </summary>
		[TestMethod]
		public void GetPriceForQuantity_Bulk()
		{
			Product product = new Product("A", (decimal)5, 6);
			decimal expected = 5;

			decimal actual = product.GetBulkPriceOfSpecificQuantity(6);

			Assert.AreEqual(expected, actual, "Bulk price incorrect for quantity: 6");
		}

		/// <summary>
		/// Test for getting the price for bulkPrice = 2
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(KeyNotFoundException))]
		public void GetPriceForQuantity_WhenQuantityNotFound_ShouldThrowKeyNotFoundException()
		{
			Product product = new Product("A", (decimal)1.25);

			decimal actual = product.GetBulkPriceOfSpecificQuantity(2);

			// Assert is handled by the ExpectedException attribute on the test method.
		}
	}

	/// <summary>
	/// Tests for function AddPrice(price, quantity)
	/// </summary>
	[TestClass]
	public class AddPriceTest
	{
		[TestMethod]
		public void AddPrice_UpdatesProductPrices()
		{
			Product product = new Product("A", (decimal)1.25);
			product.AddPrice((decimal)3, 3);
			product.AddPrice((decimal)5, 6);

			decimal expected1 = (decimal)1.25;
			decimal expected3 = (decimal)3;
			decimal expected6 = (decimal)5;
			decimal actual1 = product.GetBulkPriceOfSpecificQuantity(1);
			decimal actual3 = product.GetBulkPriceOfSpecificQuantity(3);
			decimal actual6 = product.GetBulkPriceOfSpecificQuantity(6);

			Assert.AreEqual(expected1, actual1, "AddPrice incorrectly updates product prices for quantity: 1");
			Assert.AreEqual(expected3, actual3, "AddPrice incorrectly updates product prices for quantity: 3");
			Assert.AreEqual(expected6, actual6, "AddPrice incorrectly updates product prices for quantity: 6");
		}
	}
}
