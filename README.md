# Project Title

Point Of Sale

## Getting Started

Point of sale scanning system library that accepts an arbitrary ordering of products, and returns the total price for an entire shopping cart based on per unit or volume prices as applicable.

### Prerequisites

Program is written in C#.
Install nuget package: Newtonsoft.Json v12.0.1

### Installing

* Fork the git repository.
* Open Program.sln.
* Press Start (F5)
* An example using the library is in the Program project.
* Add products to scan in Program > Program.cs using the ScanProduct() function 

``` 
string productCode = "A";
double price = 1.0;
int quantity = 1;

PointOfSaleTerminal terminal = new PointOfSaleTerminal();
terminal.SetPricing(productCode, price, quantity);
terminal.ScanProduct("A");
decimal result = terminal.CalculateTotal(); 

Console.WriteLine("total: $" + result); // should return $1 in the Console
Console.WriteLine("Press any key to continue.");
Console.ReadKey();
```

* You can also update the prices and products from a JSON file in Program > Input > register.json

```
DataLoader loader = new DataLoader();
List<ProductData> productDatas = loader.LoadJson("../../Input/register.json");

PointOfSaleTerminal terminal2 = new PointOfSaleTerminal();

foreach (var productData in productDatas)
{
    terminal2.SetPricing(productData.productCode, productData.price, productData.quantity);
}

string productCodesToScan = "ABCDABA";
foreach(var pdtCode in productCodesToScan)
{
    terminal2.ScanProduct(pdtCode.ToString());
}

decimal result1 = terminal2.CalculateTotal();
Console.WriteLine("total1: $" + result1); // should return $13.25 based on prices from given register.json 
Console.WriteLine("Press any key to continue.");
Console.ReadKey();
```

## Running the tests
Go to Test > Windows > Test Explorer.
Click 'Run All' in the Test Explorer window.

### Under namespace ProductTests,
* CalculateProduct_MinItemsScannedNotMet_GivesCorrectTotal_AndThrowsWarning()
* Example Scenario:
```
Prices: 3 units for $1 or 5 units for $1.50
Scanned: 6 units
Total will be [5 units=$1.50, with warning shown for last unit]
There are many ways to calculate the price.

* Option 1
Always calculate all possible prices, and return the lowest possible minimum as the subtotal.
Eg. Total will be [2*3units=$2, with warning shown for last unit]
Drawback(s): May unnecessarily increase complexity of CalculateSubTotal() method. 

* Option 2
Always calculate based on max bulk price first, and slowly whittle down to min bulk price.
Eg. Total will be [5 units=$1.50, with warning shown for last unit]
Drawback(s): May result in inaccurate subtotal.

* Option 3
Always calculate based on max bulk price first, then divide the remaining quantity by the min bulk price.
Eg. Total will be [5 units=$1.50, 1 unit=$0.34]
Drawback(s): Client will not be able to restrict minimum quantities purchasable.

* Option 4
Ensure there must always be a price for quantity=1 for every product.
Drawback(s): When registering the product, the price for quantity=1 must always be set first. This reduces flexibility for order of setting product prices.
```
* Option 2 was implemented, but in the real-world would consult with the Client to find out which option they would need.

## Authors

* **Qi Min Ser** 

## Acknowledgments

* Readme file base copied from: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
* .gitignore file copied from: https://gist.github.com/kmorcinek/2710267