﻿using System;
using System.Collections.Generic;
using System.IO;
using PointOfSale;

namespace Program
{
	class Program
	{
		/// <summary>
		/// Application that uses the PointOfSale library
		/// </summary>
		static void Main()
		{
			DataLoader loader = new DataLoader();
			List<ProductData> productDatas = loader.LoadJson("../../Input/register.json");

			PointOfSaleTerminal terminal = new PointOfSaleTerminal();

			foreach (var productData in productDatas)
			{
				terminal.SetPricing(productData.productCode, productData.price, productData.quantity);
			}
			//scenario 1, expected = $13.25
			string productCodesToScan = "ABCDABA";
			foreach(var productCode in productCodesToScan)
			{
				terminal.ScanProduct(productCode.ToString());
			}
			decimal result1 = terminal.CalculateTotal();

			//scenario 2, expected = $6
			string productCodesToScan2 = "CCCCCCC";
			foreach (var productCode in productCodesToScan2)
			{
				terminal.ScanProduct(productCode.ToString());
			}
			decimal result2 = terminal.CalculateTotal();

			//scenario 3, expected = $7.25
			string productCodesToScan3 = "ABCD";
			foreach (var productCode in productCodesToScan3)
			{
				terminal.ScanProduct(productCode.ToString());
			}
			decimal result3 = terminal.CalculateTotal();

			Console.WriteLine("total1: $" + result1);
			Console.WriteLine("total2: $" + result2);
			Console.WriteLine("total3: $" + result3);
			Console.WriteLine("Press any key to exit.");
			Console.ReadKey();
		}
	}
}
